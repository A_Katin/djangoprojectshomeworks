from django.shortcuts import render
from django.http import HttpResponse, HttpRequest


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Main page!')


def show(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Hello LESSON_1 page!')
