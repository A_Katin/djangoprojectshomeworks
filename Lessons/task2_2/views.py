from django.shortcuts import render
from django.http import HttpResponse, HttpRequest


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse(f'Home page homework "Lesson2 - task2_2 index-view"!')


def bio(request: HttpRequest, username: str = '') -> HttpResponse:
    if username:
        return HttpResponse(f'Page homework "Lesson2 - task2_2 - username is {username}"!')
    else:
        return HttpResponse(f'Home page homework "Lesson2 - task2_2"!')
