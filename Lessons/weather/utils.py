import requests
import json

API_KEY_TO_OPEN_WEATHER = '5f379f80a889e986d45c40b875a14f8f'


def get_data(city):
    url = 'http://api.openweathermap.org/data/2.5/weather'
    params = {'APPID': API_KEY_TO_OPEN_WEATHER, 'q': city, 'units': 'metric', 'lang': 'ua'}
    req = requests.get(url, params=params)
    temp_data = {}
    if req.status_code == 200:
        temp_data = req.json()

    return temp_data
