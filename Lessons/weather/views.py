from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from weather.utils import get_data


def index(request: HttpRequest) -> HttpResponse:
    city = request.GET.get('city')
    if city:
        data_dict = get_data(city)
        if data_dict:
            return render(request, 'weather/home.html', {'data_dict': data_dict})
        else:
            return HttpResponse(f'<script>alert("City {city} does not exist!");</script>')
    else:
        return render(request, 'weather/home.html')
