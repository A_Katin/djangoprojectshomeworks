from django.shortcuts import render
from django.http import HttpResponse, HttpRequest


def home(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Home page homework "Lesson2 - task2_1"!')


def book(request: HttpRequest, title: str = '') -> HttpResponse:
    return HttpResponse(f'Home page homework "Lesson2 - task2_1 - title {title}"!')
