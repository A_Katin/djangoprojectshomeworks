from django.shortcuts import render
from django.http import HttpResponse, HttpRequest


def home(request: HttpRequest) -> HttpResponse:
    return HttpResponse('Home page homework "Lesson2 - redirect to lesson_2"!')
